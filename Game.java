public class Game implements Item {

    private final String aName;
    private final double aSpaceRequired; // in GB
    private int aAchievementsUnlocked = 0;
    private boolean aInstalled = false;

    public Game(String pName, double pSpaceRequired) {
        aName = pName;
        aSpaceRequired = pSpaceRequired;
    }

    @Override
    public void install() {
        aInstalled = true;
    }

    @Override
    public String getName() {
        return aName;
    }

    @Override
    public double getSpaceRequired() {
        return aSpaceRequired;
    }

    public void unlockNewAchievement() {
        aAchievementsUnlocked++;
    }

    public int getAchievementsUnlocked() {
        return aAchievementsUnlocked;
    }
}
