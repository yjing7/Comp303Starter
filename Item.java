public interface Item {

    void install();
    String getName();
    double getSpaceRequired();

}
