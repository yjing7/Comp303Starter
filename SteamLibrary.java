import java.util.ArrayList;
import java.util.List;

public class SteamLibrary {

    private final List<Item> aItems = new ArrayList<>();

    /**
     * @pre pGame != null
     */
    public void addItem(Item pItem) {
        assert pItem != null;
        aItems.add(pItem);
    }
}
